# ByoHostPool

A resource and controller to manage ByoHost creation with BareMetalPool.

## Description
When `Bring Your Own Host` Clusterapi provider is used with the broker, 
ByoHost are created by provisionning bareMetalHost with custom userData and image.

The ByoHostPool resource specifies the number of ByoHost expected.

The creation of ByoHost is done by provisioning one BareMetalHost with
a custom image/userData and an image name generated from the specification of the ByoHostPool resource.

The deletion of ByoHost is done by deleting the associated BareMetalHost, 
under the condition that this ByoHost is not attached to a ByoMachine.


## License

Copyright 2023 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

