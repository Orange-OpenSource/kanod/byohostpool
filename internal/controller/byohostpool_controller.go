/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"encoding/base64"
	"fmt"
	"os"
	"reflect"
	"sort"
	"time"

	"github.com/go-logr/logr"

	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	bmhv1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	bmpv1 "gitlab.com/Orange-OpenSource/kanod/baremetalpool/api/v1"
	byohv1beta1 "gitlab.com/Orange-OpenSource/kanod/byohostpool/internal/byoh_api/v1beta1"
	certv1 "k8s.io/api/certificates/v1"
	capiv1beta1 "sigs.k8s.io/cluster-api/api/v1beta1"

	poolsv1alpha1 "gitlab.com/Orange-OpenSource/kanod/byohostpool/api/v1alpha1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	logger "sigs.k8s.io/controller-runtime/pkg/log"
)

// ByoHostPoolReconciler reconciles a ByoHostPool object
type ByoHostPoolReconciler struct {
	client.Client
	Scheme       *runtime.Scheme
	DeletionMode bool
}

const (
	MilliResyncPeriod                   = 10000
	BYOHOSTPOOL_NAME_LABEL              = "kanod.io/byohostpool"
	BYOH_BOOTSTRAPKUBECONFIG_ANNOTATION = "kanod.io/byoh-bootstrapkubeconfig"
	BYOH_CLUSTER_NAMESPACE_ANNOTATION   = "kanod.io/byoh-cluster-namespace"
	BYHOST_PROVISIONING_TIMEOUT         = 600
	BmhDataPausedAnnotationValue        = "network.kanod.io/bmh-data"
	BmhDataVersionAnnotation            = "network.kanod.io/bmh-data-version"
	CaCertPath                          = "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
	ByoHostPoolPausedAnnotation         = "byohostpool.kanod.io/paused"
)

//+kubebuilder:rbac:groups=pools.kanod.io,resources=byohostpools,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=pools.kanod.io,resources=byohostpools/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=pools.kanod.io,resources=byohostpools/finalizers,verbs=update
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/finalizers,verbs=update
//+kubebuilder:rbac:groups=apiextensions.k8s.io,resources=customresourcedefinitions,verbs=get;list;watch;create;update;patch
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=byomachines,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=byomachines/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=byohosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=byohosts/status,verbs=get;update;patch
//+kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;update;delete
//+kubebuilder:rbac:groups=certificates.k8s.io,resources=certificatesigningrequests,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=bootstrapkubeconfigs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete

func (r *ByoHostPoolReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := logger.Log.WithValues()
	var byoHostPoolSize int32
	var statusMessage string

	r.DeletionMode = false
	mode := os.Getenv("DEPROVISIONING_DELETION_MODE")
	if mode == "true" {
		r.DeletionMode = true
	}

	byoHostPool := &poolsv1alpha1.ByoHostPool{}
	err := r.Get(ctx, req.NamespacedName, byoHostPool)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("byoHostPool resource not found. Ignoring")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Failed to get byoHostPool")
		return ctrl.Result{}, err
	}

	annotations := byoHostPool.GetAnnotations()
	if annotations != nil {
		if _, ok := annotations[ByoHostPoolPausedAnnotation]; ok {
			log.Info("byohostpool is paused")
			return ctrl.Result{Requeue: false}, nil
		}
	}

	if byoHostPool.Spec.Replicas != nil {
		byoHostPoolSize = *(byoHostPool.Spec.Replicas)
	}

	lbls := labels.Set{
		bmpv1.KanodBareMetalPoolNameLabel: byoHostPool.Spec.BareMetalPool,
	}

	bmhImage := r.generateBmhImageName(ctx, byoHostPool, log)

	bmhList := &bmhv1alpha1.BareMetalHostList{}
	err = r.List(ctx, bmhList, &client.ListOptions{
		Namespace:     byoHostPool.Namespace,
		LabelSelector: labels.SelectorFromSet(lbls),
	})

	if err != nil {
		log.Error(err, "Failed to list bmh", "selector labels", lbls)
		return ctrl.Result{}, err
	}

	requeue, err := r.removeProvisionedBmhWithoutByoHost(ctx, byoHostPool, bmhList, log)
	if err != nil {
		log.Error(err, "Failed to remove provisioned baremetalhost without byohost")
		return ctrl.Result{}, err
	}

	if requeue {
		return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
	}

	bmhCount, bmhNamesList, byoHostCount, err := r.getBmhStats(bmhList, bmhImage, log)
	if err != nil {
		log.Error(err, "Failed to get baremetalhost statistics")
		return ctrl.Result{}, err
	}

	if bmhCount == byoHostPoolSize {
		statusMessage = "byohHostPool allocated"
	}

	if bmhCount < byoHostPoolSize {
		statusMessage = "Under-allocation"
		requeue, err = r.provisionBmh(ctx, byoHostPool, log)
		if err != nil {
			return ctrl.Result{}, err
		}
		if requeue {
			return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
		}
	}

	if bmhCount > byoHostPoolSize {
		statusMessage = "Over-allocation"
		if err = r.deprovisionBmh(ctx, byoHostPool, log); err != nil {
			return ctrl.Result{}, err
		}
	}

	if err = r.updateStatus(req.NamespacedName, bmhNamesList, bmhCount, byoHostCount, statusMessage, log); err != nil {
		log.Error(err, "Failed to update status ")
		return ctrl.Result{}, err
	}

	return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *ByoHostPoolReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&poolsv1alpha1.ByoHostPool{}).
		Complete(r)
}

// getBmhStats returns provisioned baremetalhost number
func (r *ByoHostPoolReconciler) getBmhStats(
	bmhList *bmhv1alpha1.BareMetalHostList,
	bmhImage string,
	log logr.Logger,
) (int32, []string, int32, error) {
	var bmhCount int32
	var byoHostCount int32
	var bmhNamesList []string

	for _, bmh := range bmhList.Items {
		if bmh.GetObjectMeta().GetDeletionTimestamp() == nil &&
			bmh.Spec.Image != nil &&
			bmh.Spec.Image.URL == bmhImage {

			bmhCount += 1
			bmhNamesList = append(bmhNamesList, bmh.Name)

			byohostPresent, err := r.existsByoHost(context.Background(), bmh.Name, bmh.Namespace, log)
			if err != nil {
				log.Error(err, "Failed to get ByoHost information", "name", bmh.Name)
				return 0, nil, 0, err
			}
			if byohostPresent {
				byoHostCount += 1
			}
		}
	}

	return bmhCount, bmhNamesList, byoHostCount, nil
}

// provisionBmh add image to one available bareMetalHost
func (r *ByoHostPoolReconciler) provisionBmh(ctx context.Context,
	byoHostPool *poolsv1alpha1.ByoHostPool,
	log logr.Logger,
) (bool, error) {
	var requeueByoh = false
	var kubeconfig string

	byohImage := r.generateBmhImageName(ctx, byoHostPool, log)

	bmh, err := r.getAvailableBmh(ctx, byoHostPool, log)
	if err != nil {
		log.Error(err, "Failed to get candidate baremetalhost for provisioning")
		return false, err
	}

	log.Info("DEBUG - provisionBmh", "bmh", bmh)

	if bmh == nil {
		log.Info("trying to find a baremetalhost for provisioning, but no baremetalhost available")
		return false, nil
	}

	bmhName := bmh.Name
	bmhNameSpace := bmh.Namespace
	bmhAnnotations := bmh.GetAnnotations()

	// phase 1 : bootstrapkubeconfig generation

	bootstrapKubeconfigAnnotationValue, ok := bmhAnnotations[BYOH_BOOTSTRAPKUBECONFIG_ANNOTATION]
	log.Info("DEBUG - phase 1", "ok", ok, "bootstrapKubeconfigAnnotationValue", bootstrapKubeconfigAnnotationValue, "kubeconfig", kubeconfig)

	if !ok || bootstrapKubeconfigAnnotationValue == "" {
		requeueByoh, kubeconfig, err = r.buildByohContext(
			ctx, log, bmh, byoHostPool.Spec.ApiServer,
			byoHostPool.Spec.InsecureSkipTLSVerify)

		if err != nil {
			return false, err
		}

		if requeueByoh {
			return true, nil
		}
	}

	// phase 2 : add bootstrapkubeconfig in baremetalhost annotation and force bmh-data reconcile for metadata generation
	bootStrapKubeconfigData := base64.StdEncoding.EncodeToString([]byte(kubeconfig))
	log.Info("DEBUG - phase 2", "ok", ok, "bootstrapKubeconfigAnnotationValue", bootstrapKubeconfigAnnotationValue, "kubeconfig", kubeconfig)

	if bootstrapKubeconfigAnnotationValue == "" {
		err = retry.RetryOnConflict(retry.DefaultRetry,
			func() error {
				updatedBmh := &bmhv1alpha1.BareMetalHost{}
				key := client.ObjectKey{Namespace: bmhNameSpace, Name: bmhName}
				if err := r.Client.Get(context.Background(), key, updatedBmh); err != nil {
					log.Error(err, "Failed to get baremetalhost", "name", bmhName, "namespace", bmhNameSpace)
					return err
				}

				annotations := updatedBmh.GetAnnotations()
				annotations[BYOH_BOOTSTRAPKUBECONFIG_ANNOTATION] = bootStrapKubeconfigData
				annotations[BYOH_CLUSTER_NAMESPACE_ANNOTATION] = byoHostPool.Namespace
				delete(annotations, BmhDataVersionAnnotation)
				updatedBmh.SetAnnotations(annotations)

				labels := updatedBmh.GetLabels()
				labels[BYOHOSTPOOL_NAME_LABEL] = byoHostPool.Name
				updatedBmh.SetLabels(labels)

				if err = r.Client.Update(context.Background(), updatedBmh); err != nil {
					return err
				}

				return nil
			})

		if err != nil {
			log.Error(err, "Cannot update baremetalhost with new annotation")
			return false, err
		}
		return true, nil
	}

	// phase 3 : add image in baremetalhost spec
	log.Info("DEBUG - phase 3", "ok", ok, "bootstrapKubeconfigAnnotationValue", bootstrapKubeconfigAnnotationValue, "kubeconfig", kubeconfig)

	if bmh.Spec.Image == nil {
		log.Info("add image in baremetalhost spec", "bmh name", bmhName, "namespace", bmhNameSpace, "bmh labels", bmh.GetLabels())
		clusterList := &capiv1beta1.ClusterList{}
		if err := r.List(ctx, clusterList, client.InNamespace(byoHostPool.Namespace)); err != nil {
			log.Error(err, "cannot list control planes")
			return false, err
		}
		nbClusters := len(clusterList.Items)
		if nbClusters != 1 {
			err = fmt.Errorf("unexpected number of cluster(s) in namespace: %d", nbClusters)
			log.Error(err, "expected one cluster in namespace", "namespace", byoHostPool.Namespace, "nb clusters", nbClusters)
			return false, err
		}

		cluster := clusterList.Items[0]

		err = retry.RetryOnConflict(retry.DefaultRetry,
			func() error {
				updatedBmh := &bmhv1alpha1.BareMetalHost{}
				key := client.ObjectKey{Namespace: bmhNameSpace, Name: bmhName}
				if err := r.Client.Get(context.Background(), key, updatedBmh); err != nil {
					log.Error(err, "Failed to get baremetalhost", "name", bmhName, "namespace", bmhNameSpace)
					return err
				}
				if updatedBmh.Spec.Image == nil {
					image := bmhv1alpha1.Image{}
					image.URL = byohImage
					image.Checksum = fmt.Sprintf("%s.md5", byohImage)
					imgFormat := byoHostPool.Spec.ByohImageFormat
					image.DiskFormat = &imgFormat
					updatedBmh.Spec.Image = &image
				}
				updatedBmh.Spec.Online = true

				updatedBmh.OwnerReferences = []metav1.OwnerReference{
					ownedByCluster(&cluster),
				}
				err = r.Client.Update(context.Background(), updatedBmh)
				if err != nil {
					return err
				}
				return nil
			})

		if err != nil {
			log.Error(err, "Cannot update baremetalhost with new image")
			return false, err
		}
	}
	log.Info("provisioning a baremetalhost", "bmh name", bmh.Name, "namespace", bmh.Namespace, "bmh labels", bmh.GetLabels())
	return false, nil
}

// deprovisionBmh
func (r *ByoHostPoolReconciler) deprovisionBmh(ctx context.Context,
	byoHostPool *poolsv1alpha1.ByoHostPool,
	log logr.Logger,
) error {
	bmh, err := r.getBmhToDelete(ctx, byoHostPool, log)
	if err != nil {
		log.Error(err, "Failed to get candidate baremetalhost for deprovisioning")
		return err
	}

	if bmh == nil {
		log.Info("trying to deprovision a baremetalhost, but no candidate baremetalhost")
		return nil
	}

	err = r.removeBmhByohResources(ctx, bmh.Name, bmh.Namespace, log)
	if err != nil {
		log.Error(err, "cannot remove ByoHost / BareMetalHost")
		return err
	}

	log.Info("remove ByoHost / BareMetalHost resources", "name", bmh.Name, "namespace", bmh.Namespace)

	return nil
}

// removeBmhByohResources
func (r *ByoHostPoolReconciler) removeBmhByohResources(
	ctx context.Context,
	rsName string,
	rsNamespace string,
	log logr.Logger,
) error {
	var err error

	err = retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			if r.DeletionMode {
				err = r.removeBareMetalHost(ctx, rsName, rsNamespace, log)
			} else {
				err = r.removeImageInBaremetalhost(ctx, rsName, rsNamespace, log)
			}
			if err != nil {
				log.Error(err, "Failed to delete BareMetalHost", "name", rsName)
				return err
			}

			if err = r.removeByohost(ctx, rsName, rsNamespace, log); err != nil {
				log.Error(err, "Failed to remove ByoHost / BootstrapKubeconfig / Csr resources", "name", rsName)
				return err
			}
			log.Info("remove BootstrapKubeconfig / Csr", "name", rsName, "namespace", rsNamespace)
			return nil
		})
	if err != nil {
		log.Error(err, "cannot remove ByoHost / BootstrapKubeconfig / Csr")
		return err
	}
	return nil
}

// getAvailableBmh get one available bmh
func (r *ByoHostPoolReconciler) getAvailableBmh(ctx context.Context,
	byoHostPool *poolsv1alpha1.ByoHostPool,
	log logr.Logger,
) (*bmhv1alpha1.BareMetalHost, error) {

	lbls := labels.Set{
		bmpv1.KanodBareMetalPoolNameLabel: byoHostPool.Spec.BareMetalPool,
	}

	log.Info("DEBUG - getAvailableBmh", "lbls", lbls)

	bmhList := &bmhv1alpha1.BareMetalHostList{}
	err := r.List(ctx, bmhList, &client.ListOptions{
		Namespace:     byoHostPool.Namespace,
		LabelSelector: labels.SelectorFromSet(lbls),
	})
	if err != nil {
		log.Error(err, "Failed to list bmh", "selector labels", lbls)
		return nil, err
	}

	log.Info("DEBUG - getAvailableBmh", "bmhList.Items", bmhList.Items)

	sort.SliceStable(bmhList.Items, func(i, j int) bool { return bmhList.Items[i].Name < bmhList.Items[j].Name })

	var candidateBmh *bmhv1alpha1.BareMetalHost

	for _, bmh := range bmhList.Items {
		if bmh.GetObjectMeta().GetDeletionTimestamp() == nil &&
			bmh.Spec.Image == nil &&
			(bmh.Status.Provisioning.State == bmhv1alpha1.StateReady ||
				bmh.Status.Provisioning.State == bmhv1alpha1.StateAvailable) {

			candidateBmh = &bmh
			break
		}
	}

	return candidateBmh, nil
}

func (r *ByoHostPoolReconciler) getBmhToDelete(ctx context.Context,
	byoHostPool *poolsv1alpha1.ByoHostPool,
	log logr.Logger,
) (*bmhv1alpha1.BareMetalHost, error) {

	lbls := labels.Set{
		bmpv1.KanodBareMetalPoolNameLabel: byoHostPool.Spec.BareMetalPool,
		BYOHOSTPOOL_NAME_LABEL:            byoHostPool.Name,
	}

	bmhList := &bmhv1alpha1.BareMetalHostList{}
	err := r.List(ctx, bmhList, &client.ListOptions{
		Namespace:     byoHostPool.Namespace,
		LabelSelector: labels.SelectorFromSet(lbls),
	})
	if err != nil {
		log.Error(err, "Failed to list bmh", "selector labels", lbls)
		return nil, err
	}

	var candidateBmh *bmhv1alpha1.BareMetalHost
	for _, bmh := range bmhList.Items {
		if bmh.GetObjectMeta().GetDeletionTimestamp() == nil &&
			bmh.Spec.Image != nil && bmh.Status.Provisioning.State == bmhv1alpha1.StateProvisioned {

			isAttachedToMachines, err := r.isByoHostAttachedToMachine(ctx, bmh.Name, bmh.Namespace, log)
			if err != nil {
				log.Error(err, "Failed to get ByoHost information", "name", bmh.Name)
				return nil, err
			}

			if !isAttachedToMachines {
				candidateBmh = &bmh
				break
			}
		}
	}

	return candidateBmh, nil
}

// updateStatus update the ByoHostPool status
func (r *ByoHostPoolReconciler) updateStatus(
	key types.NamespacedName,
	bmhNames []string,
	bmhCount int32,
	byoHostCount int32,
	message string,
	log logr.Logger,
) error {
	ctx := context.Background()

	status := poolsv1alpha1.ByoHostPoolStatus{
		Replicas:     bmhCount,
		BmhNamesList: bmhNames,
		NbByoHost:    byoHostCount,
		Status:       message,
	}

	byoHostPool := &poolsv1alpha1.ByoHostPool{}

	err := retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			if err := r.Get(ctx, key, byoHostPool); err != nil {
				return err
			}
			if !reflect.DeepEqual(byoHostPool.Status, status) {
				byoHostPool.Status = status
				return r.Status().Update(ctx, byoHostPool)
			}
			return nil
		})

	if err != nil {
		log.Error(err, "failed to update baremetalpoolStatus")
	}
	return err
}

func (r *ByoHostPoolReconciler) generateBmhImageName(
	ctx context.Context,
	byoHostPool *poolsv1alpha1.ByoHostPool,
	log logr.Logger,
) string {

	byohOs := byoHostPool.Spec.ByohOs
	byohOsVersion := byoHostPool.Spec.ByohOsVersion
	byohImageFormat := byoHostPool.Spec.ByohImageFormat
	byohRepository := byoHostPool.Spec.ByohRepository
	k8sVersion := byoHostPool.Spec.K8sVersion

	byohImage := fmt.Sprintf("%s/repository/kanod/kanod/%s-%s/%s/%s-%s-%s.%s",
		byohRepository, byohOs, k8sVersion, byohOsVersion, byohOs, k8sVersion, byohOsVersion, byohImageFormat)

	return byohImage
}

func (r *ByoHostPoolReconciler) removeBootstrapkubeconfigCsr(
	ctx context.Context,
	byoHostName string,
	byoHostNamespace string,
	log logr.Logger,
) error {
	bootstrapKubeconfig := &byohv1beta1.BootstrapKubeconfig{}
	bootstrapKubeconfigName := fmt.Sprintf("%s-byohpool-kubeconfig", byoHostName)
	namespaceNamed := types.NamespacedName{Name: bootstrapKubeconfigName, Namespace: byoHostNamespace}

	log.Info("removing bootstrapKubeconfig", "name", bootstrapKubeconfigName, "namespace", byoHostNamespace)

	err := r.Get(ctx, namespaceNamed, bootstrapKubeconfig)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("bootstrapKubeconfig resource not found. Ignoring")
		} else {
			log.Error(err, "Failed to get bootstrapKubeconfig", "name", bootstrapKubeconfigName, "namespace", byoHostNamespace)
			return err
		}
	} else {
		err = r.Delete(ctx, bootstrapKubeconfig)
		if err != nil {
			log.Error(err, "Cannot delete bootstrapKubeconfig", "name", bootstrapKubeconfigName, "namespace", byoHostNamespace)
			return err
		}
	}

	certSignReq := &certv1.CertificateSigningRequest{}
	csrName := fmt.Sprintf("byoh-csr-%s", byoHostName)
	csrNamespaceNamed := types.NamespacedName{Name: csrName, Namespace: ""}

	log.Info("removing csr associated with byoHost", "csr name", csrName, " byoHost name", byoHostName, " byoHost namespace", byoHostNamespace)

	err = r.Get(ctx, csrNamespaceNamed, certSignReq)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("csr resource not found. Ignoring", "name", csrName)
		} else {
			log.Error(err, "Failed to get csr for byoHost", "name", csrName)
			return err
		}
	} else {
		if err = r.Delete(ctx, certSignReq); err != nil {
			log.Error(err, "Cannot delete csr for byoHost", "name", csrName)
			return err
		}
	}

	return nil
}

func (r *ByoHostPoolReconciler) removeByohost(
	ctx context.Context,
	byoHostName string,
	byoHostNamespace string,
	log logr.Logger,
) error {
	log.Info("removing byoHost", "name", byoHostName, "namespace", byoHostNamespace)

	byoHost := &byohv1beta1.ByoHost{}
	namespaceNamed := types.NamespacedName{Name: byoHostName, Namespace: byoHostNamespace}
	err := r.Get(ctx, namespaceNamed, byoHost)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("byoHost resource not found. Ignoring")
		} else {
			log.Error(err, "Failed to get byoHost", "name", byoHostName, "namespace", byoHostNamespace)
			return err
		}
	} else {
		err = r.Delete(ctx, byoHost)
		if err != nil {
			log.Error(err, "Cannot delete byoHost", "name", byoHost.Name, "namespace", byoHost.Namespace)
			return err
		}
	}

	if err = r.removeBootstrapkubeconfigCsr(ctx, byoHostName, byoHostNamespace, log); err != nil {
		log.Error(err, "Cannot delete Bootstrapkubeconfig / Csr", "name", byoHost.Name, "namespace", byoHost.Namespace)
		return err
	}

	return nil
}

func (r *ByoHostPoolReconciler) isByoHostAttachedToMachine(
	ctx context.Context,
	byoHostName string,
	byoHostNamespace string,
	log logr.Logger,
) (bool, error) {
	isAttachedToMachine := false

	byoHost := &byohv1beta1.ByoHost{}
	namespaceNamed := types.NamespacedName{Name: byoHostName, Namespace: byoHostNamespace}
	err := r.Get(ctx, namespaceNamed, byoHost)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("byoHost resource not found. Ignoring")
			return isAttachedToMachine, nil
		}
		log.Error(err, "Failed to get byoHost", "name", byoHostName, "namespace", byoHostNamespace)
		return isAttachedToMachine, err
	}

	if byoHost.Status.MachineRef != nil {
		isAttachedToMachine = true
	}

	return isAttachedToMachine, nil
}

func (r *ByoHostPoolReconciler) existsByoHost(
	ctx context.Context,
	byoHostName string,
	byoHostNamespace string,
	log logr.Logger,
) (bool, error) {
	byoHost := &byohv1beta1.ByoHost{}
	namespaceNamed := types.NamespacedName{Name: byoHostName, Namespace: byoHostNamespace}
	if err := r.Get(ctx, namespaceNamed, byoHost); err != nil {
		if k8serrors.IsNotFound(err) {
			return false, nil
		}
		log.Error(err, "Failed to get byoHost", "name", byoHostName, "namespace", byoHostNamespace)
		return false, err
	}

	return true, nil
}

func (r *ByoHostPoolReconciler) removeBareMetalHost(
	ctx context.Context,
	bmhName string,
	bmhNamespace string,
	log logr.Logger,
) error {
	log.Info("removing BareMetalHost", "name", bmhName, "namespace", bmhNamespace)

	bareMetalHost := &bmhv1alpha1.BareMetalHost{}
	namespaceNamed := types.NamespacedName{Name: bmhName, Namespace: bmhNamespace}

	if err := r.Get(ctx, namespaceNamed, bareMetalHost); err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("BareMetalHost resource not found. Ignoring")
		} else {
			log.Error(err, "Failed to get BareMetalHost", "name", bmhName, "namespace", bmhNamespace)
			return err
		}
	} else {
		err = r.Delete(ctx, bareMetalHost)
		if err != nil {
			log.Error(err, "Cannot delete BareMetalHost", "name", bmhName, "namespace", bmhNamespace)
			return err
		}
	}
	return nil
}

func (r *ByoHostPoolReconciler) removeProvisionedBmhWithoutByoHost(ctx context.Context,
	byoHostPool *poolsv1alpha1.ByoHostPool,
	bmhList *bmhv1alpha1.BareMetalHostList,
	log logr.Logger,
) (bool, error) {
	requeue := false
	for _, bmh := range bmhList.Items {
		var duration float64
		operationalError := false

		if bmh.Status.OperationalStatus == "error" {
			operationalError = true
			log.Info("operational error detected for baremetalhost", "name", bmh.Name, "namespace", bmh.Namespace)
		}

		if bmh.GetObjectMeta().GetDeletionTimestamp() == nil &&
			bmh.Spec.Image != nil &&
			bmh.Status.Provisioning.State == bmhv1alpha1.StateProvisioned {
			provisionStartTime := bmh.Status.OperationHistory.Provision.Start.Time
			now := metav1.Now()
			duration = now.Sub(provisionStartTime).Seconds()
		}

		byohostPresent, err := r.existsByoHost(ctx, bmh.Name, bmh.Namespace, log)
		if err != nil {
			log.Error(err, "Failed to get ByoHost information", "name", bmh.Name)
			return requeue, err
		}

		if !byohostPresent && duration > BYHOST_PROVISIONING_TIMEOUT {
			log.Info(fmt.Sprintf("baremetalhost provisioned since %f seconds but no byohost initiated", duration), "name", bmh.Name, "namespace", bmh.Namespace)
		}

		if operationalError || (!byohostPresent && duration > BYHOST_PROVISIONING_TIMEOUT) {
			if err = r.removeBmhByohResources(ctx, bmh.Name, bmh.Namespace, log); err != nil {
				log.Error(err, "cannot remove ByoHost / BareMetalHost")
				return requeue, err
			}
			requeue = true
			return requeue, nil
		}
	}
	return requeue, nil
}

func (r *ByoHostPoolReconciler) removeImageInBaremetalhost(
	ctx context.Context,
	bmhName string,
	bmhNamespace string,
	log logr.Logger,
) error {
	log.Info("removing image from BareMetalHost spec", "name", bmhName, "namespace", bmhNamespace)

	err := retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			updatedBmh := &bmhv1alpha1.BareMetalHost{}
			key := client.ObjectKey{Namespace: bmhNamespace, Name: bmhName}
			if err := r.Client.Get(context.Background(), key, updatedBmh); err != nil {
				log.Error(err, "Failed to get baremetalhost", "name", bmhName, "namespace", bmhNamespace)
				return err
			}

			annotations := updatedBmh.GetAnnotations()
			delete(annotations, BYOH_BOOTSTRAPKUBECONFIG_ANNOTATION)
			delete(annotations, BYOH_CLUSTER_NAMESPACE_ANNOTATION)

			updatedBmh.SetAnnotations(annotations)
			labels := updatedBmh.GetLabels()
			delete(labels, BYOHOSTPOOL_NAME_LABEL)
			updatedBmh.SetLabels(labels)

			updatedBmh.Spec.Image = nil
			updatedBmh.Spec.MetaData = nil
			updatedBmh.OwnerReferences = nil
			updatedBmh.Spec.Online = false

			if err := r.Client.Update(context.Background(), updatedBmh); err != nil {
				return err
			}
			return nil
		})

	if err != nil {
		log.Error(err, "Cannot remove image from BareMetalHost", "name", bmhName, "namespace", bmhNamespace)
		return err
	}
	return nil
}

func (r *ByoHostPoolReconciler) buildByohContext(
	ctx context.Context,
	log logr.Logger,
	bareMetalHost *bmhv1alpha1.BareMetalHost,
	apiServer string,
	insecureSkipTLSVerify bool,
) (bool, string, error) {
	var bootstrapKubeconfig = &byohv1beta1.BootstrapKubeconfig{}
	name := fmt.Sprintf("%s-byohpool-kubeconfig", bareMetalHost.Name)
	key := types.NamespacedName{Name: name, Namespace: bareMetalHost.Namespace}
	err := r.Get(ctx, key, bootstrapKubeconfig)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			caCert, err := os.ReadFile(CaCertPath)
			if err != nil {
				log.Error(err, "cannot get own ca certificate")
				return true, "", err
			}
			ba64CaCert := base64.StdEncoding.EncodeToString(caCert)
			bootstrapKubeconfig.Name = name
			bootstrapKubeconfig.Namespace = bareMetalHost.Namespace
			bootstrapKubeconfig.OwnerReferences = []metav1.OwnerReference{
				ownedByBmh(bareMetalHost),
			}
			bootstrapKubeconfig.Spec = byohv1beta1.BootstrapKubeconfigSpec{
				APIServer:                apiServer,
				InsecureSkipTLSVerify:    insecureSkipTLSVerify,
				CertificateAuthorityData: ba64CaCert,
			}
			if err = r.Client.Create(ctx, bootstrapKubeconfig); err != nil {
				log.Error(err, "cannot create BYOH bootstrap kubeconfig")
				return true, "", err
			}
			log.Info("Created BYOH Bootstrap Config")
			return true, "", nil
		} else {
			log.Error(err, "cannot get BYOH Kubeconfig")
			return true, "", err
		}
	}
	configData := bootstrapKubeconfig.Status.BootstrapKubeconfigData
	if configData == nil {
		return true, "", nil
	}

	log.Info("get BYOH BootstrapKubeconfigData")
	return false, *configData, nil
}

func ownedByBmh(bmh *bmhv1alpha1.BareMetalHost) metav1.OwnerReference {
	return metav1.OwnerReference{
		APIVersion: bmh.APIVersion,
		Kind:       bmh.Kind,
		Name:       bmh.Name,
		UID:        bmh.UID,
	}
}

func ownedByCluster(cluster *capiv1beta1.Cluster) metav1.OwnerReference {
	return metav1.OwnerReference{
		APIVersion: cluster.APIVersion,
		Kind:       cluster.Kind,
		Name:       cluster.Name,
		UID:        cluster.UID,
	}
}
