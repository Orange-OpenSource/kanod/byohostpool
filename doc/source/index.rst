===========
Byohostpool
===========

.. toctree::
    :maxdepth: 2

Overview
========

When `Bring Your Own Host` Clusterapi provider is used with the broker, 
ByoHost are created by provisionning bareMetalHost with custom userData and image.

The ByoHostPool resource specifies the number of ByoHost expected (*replicas* field).

When the number of byoHost is not equal to the *replicas* field in the ByoHostPool specification, 
byoHost are created / deleted consequently. 

The creation of ByoHost is done by provisioning one BareMetalHost with
custom image and userData. The image name is generated from information
if the specification of the ByoHostPool resource.

The deletion of ByoHost is done by deleting the associated BareMetalHost, 
under the condition that this ByoHost is not attached to a ByoMachine.
