/*
Copyright 2023 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ByoHostPoolSpec defines the desired state of ByoHostPool
type ByoHostPoolSpec struct {
	// BareMetalPool used for managing the BareMetalHost
	BareMetalPool string `json:"bareMetalPool"`
	// K8s Version of the provisioning image for BareMetalHost
	K8sVersion string `json:"k8sVersion"`
	// Number of provisioned BareMetalHost with ByoHost image
	Replicas *int32 `json:"replicas,omitempty"`
	// os name of the provisioning image for BareMetalHost
	ByohOs string `json:"byohOs"`
	// os version of the provisioning image for BareMetalHost
	ByohOsVersion string `json:"byohOsVersion"`
	// format of the provisioning image for BareMetalHost
	ByohImageFormat string `json:"byohImageFormat"`
	// repository of the provisioning image for BareMetalHost
	ByohRepository string `json:"byohRepository"`
	// api server address
	ApiServer string `json:"apiServer"`
	// insecureSkipTLSVerify mode for generating bootstrapkubeconfig
	InsecureSkipTLSVerify bool `json:"insecureSkipTLSVerify"`
}

// ByoHostPoolStatus defines the observed state of ByoHostPool
type ByoHostPoolStatus struct {
	// List of the provisioned BareMetalHost with ByoHost image
	BmhNamesList []string `json:"bmhNamesList,omitempty"`
	// Status of the byohpool
	Status string `json:"status"`
	// Number of provisioned BareMetalHost with ByoHost image
	Replicas int32 `json:"replicas"`
	// Number of provisioned BareMetalHost with ByoHost available
	NbByoHost int32 `json:"nbByoHost"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
// +kubebuilder:subresource:scale:specpath=.spec.replicas,statuspath=.status.replicas
// +kubebuilder:printcolumn:name="BareMetalPool",type="string",JSONPath=`.spec.bareMetalPool`
// +kubebuilder:printcolumn:name="K8sVersion",type="string",JSONPath=`.spec.k8sVersion`
// +kubebuilder:printcolumn:name="Replicas",type="string",JSONPath=`.spec.replicas`
// +kubebuilder:printcolumn:name="Available",type="string",JSONPath=`.status.nbByoHost`
// +kubebuilder:printcolumn:name="NbBmh",type="string",JSONPath=`.status.replicas`
// +kubebuilder:printcolumn:name="Status",type="string",JSONPath=`.status.status`

// ByoHostPool is the Schema for the byohostpools API
type ByoHostPool struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ByoHostPoolSpec   `json:"spec,omitempty"`
	Status ByoHostPoolStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ByoHostPoolList contains a list of ByoHostPool
type ByoHostPoolList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ByoHostPool `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ByoHostPool{}, &ByoHostPoolList{})
}
